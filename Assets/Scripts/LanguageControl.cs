﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageControl : MonoBehaviour
{

    [SerializeField]
    private Text _titleLableText;
    [SerializeField]
    private Text _titlePlaceholder;
    [SerializeField]
    private Text _typeText;
    [SerializeField]
    private InputField _selectItemTypeInputfield;
    [SerializeField]
    private Text _descriptionText;
    [SerializeField]
    private Text _descriptionPlaceholder;
    [SerializeField]
    private Text _effonText;
    [SerializeField]
    private Text _effonPlaceholder;
    [SerializeField]
    private Text _titleCheckBox;
    [SerializeField]
    private Text _saveButtonText;
    [SerializeField]
    private Text _cancelButtonText;
    [SerializeField]
    private Dropdown _typeDropBox;
    [SerializeField]
    private Text[] _errorsListText;

    private void Awake()
    {
        EventManager.AddListener(EventManager.CHANGED, LangChangedEventManager);
    }
    void Start()
    {
        SetRuLang();

    }
    private void LangChangedEventManager(EventData idx)
    {
        if ((int)idx.data == 0)
        {
            SetRuLang();
            _typeDropBox.RefreshShownValue();
        }
        if ((int)idx.data == 1)
        {
            SetEngLang();
            _typeDropBox.RefreshShownValue();
        }
    }
    private void SetRuLang()
    {
        _titleLableText.text = "Название";
        _titlePlaceholder.text = "Введите текст...";
        _typeText.text = "Тип";
        _descriptionText.text = "Описание";
        _descriptionPlaceholder.text = "Введите текст...";
        _effonText.text = "Усилие";
        _effonPlaceholder.text = "Введите текст...";
        _titleCheckBox.text = " Принять условия";
        _saveButtonText.text = "Сохранить";
        _cancelButtonText.text = "Отменить";
        _typeDropBox.options[0].text = "Выбрать";
        _typeDropBox.options[1].text = "Разработка";
        _typeDropBox.options[2].text = "Дизайн";
        _typeDropBox.options[3].text = "Маркетинг";
        _typeDropBox.value = 0;
        _selectItemTypeInputfield.text = "";
        foreach(var item in _errorsListText)
        {
            item.text = "обязательное поле";
        }
    }
    private void SetEngLang()
    {
        _titleLableText.text = "Title";
        _titlePlaceholder.text = "Enter text...";
        _typeText.text = "Type";
        _descriptionText.text = "Description";
        _descriptionPlaceholder.text = "Enter text...";
        _effonText.text = "Effon";
        _effonPlaceholder.text = "Enter text...";
        _titleCheckBox.text = " Terms Accepted";
        _saveButtonText.text = "Save";
        _cancelButtonText.text = "Cancel";
        _typeDropBox.options[0].text = "Select";
        _typeDropBox.options[1].text = "Development";
        _typeDropBox.options[2].text = "Design";
        _typeDropBox.options[3].text = "Marketing";
        _typeDropBox.value = 0;
        _selectItemTypeInputfield.text = "";
        foreach (var item in _errorsListText)
        {
            item.text = "required field";
        }
    }
    private void OnDestroy()
    {
        EventManager.RemoveEvent(EventManager.CHANGED, LangChangedEventManager);
    }

}
