﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    static public class EventManager
    {

        static public string CHANGED = "changed";
        static public string CHANGED_INPUT = "changed_input";

        static private Dictionary<string, List<Action<EventData>>> _pool = new Dictionary<string, List<Action<EventData>>>();

        static public void AddListener(string eventName, Action<EventData> callback)
        {
            if (_pool.ContainsKey(eventName))
            {
                if (_pool[eventName].Contains(callback))
                    return;

                _pool[eventName].Add(callback);
                return;
            }
            List<Action<EventData>> l = new List<Action<EventData>>();
            l.Add(callback);
            _pool.Add(eventName, l);

        }
        static public bool HasListenner(string eventName, Action<EventData> callback)
        {
            if (!_pool.ContainsKey(eventName))
                return false;
            if (!_pool[eventName].Contains(callback))
                return false;

            return true;
        }
        static public void DispatchEvent(string eventName, EventData data)
        {
            if (!_pool.ContainsKey(eventName))
            {
                return;
            }

            List<Action<EventData>> lAction = _pool[eventName];

            for (int i = 0; i < lAction.Count; i++)
            {
                lAction[i](data);
            }
        }

        static public bool RemoveEvent(string eventName, Action<EventData> callback)
        {

            if (!_pool.ContainsKey(eventName))
            {
                return false;
            }

            List<Action<EventData>> lAction = _pool[eventName];
            for (int i = 0; i < lAction.Count; i++)
            {
                if (lAction[i] == callback)
                {
                    lAction.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
    }
}
