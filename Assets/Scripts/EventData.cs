﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class EventData
    {
        public Object data { get;}

        public EventData(Object _data)
        {
            data = _data;
        }

    }
}
