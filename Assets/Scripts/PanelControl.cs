﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelControl : MonoBehaviour
{
    //[SerializeField]
    //private InputField _selectedItemTypeInputField;

    [SerializeField]
    private Button _cancelButton;

    [SerializeField]
    private Button _saveButton;

    [SerializeField]
    private Toggle _acceptedCheckBox;

    [SerializeField]
    private InputField _titleInputfield;
    [SerializeField]
    private Text _titleErrorLabel;

    [SerializeField]
    private InputField _descriptionInputfield;
    [SerializeField]
    private Text _descriptionErrorLabel;

    [SerializeField]
    private InputField _effortInputfield;
    [SerializeField]
    private Text _effortErrorLabel;

    [SerializeField]
    private Dropdown _typeDropbox;
    [SerializeField]
    private Text _typeErrorLabel;
    [SerializeField]
    private InputField _typeInputfield;
    [SerializeField]
    private GameObject _okInSaveBG;
    [SerializeField]
    private Button _okButton;

    [SerializeField]
    private Dropdown _languageDropbox;


    void Start()
    {        
        AddListeners();
    }
    private void AddListeners()
    {
        _languageDropbox.onValueChanged.AddListener(LangChanged);
        _acceptedCheckBox.onValueChanged.AddListener(AcceptedTglChanged);
        _cancelButton.onClick.AddListener(CancelClick);
        _saveButton.onClick.AddListener(SaveClick);
        _typeDropbox.onValueChanged.AddListener(DropDownTypeChanged);
        _okButton.onClick.AddListener(OkClick);
    }
    private  void LangChanged(int idx)
    {
        EventManager.DispatchEvent(EventManager.CHANGED, new EventData(idx));
    }
    private void Update()
    {
        if(_titleInputfield.text!="")
            _titleErrorLabel.gameObject.SetActive(false);
        if(_typeInputfield.text!="")
            _typeErrorLabel.gameObject.SetActive(false);
        if(_descriptionInputfield.text!="")
            _descriptionErrorLabel.gameObject.SetActive(false);
        if(_effortInputfield.text!="")
            _effortErrorLabel.gameObject.SetActive(false);

    }

    private void OkClick()
    {
        _okInSaveBG.SetActive(false);
        Cancel();
    }
    private void AcceptedTglChanged(bool ch)
    {
        _saveButton.interactable = ch;
    }

    private void CancelClick()
    {
        Cancel();
    }
    private void Cancel()
    {
        _titleInputfield.text = "";
        _descriptionInputfield.text = "";
        _effortInputfield.text = "";
        _typeDropbox.value = 0;
        _saveButton.interactable = false;
        _acceptedCheckBox.isOn = false;
        _titleErrorLabel.gameObject.SetActive(false);
        _typeErrorLabel.gameObject.SetActive(false);
        _descriptionErrorLabel.gameObject.SetActive(false);
        _effortErrorLabel.gameObject.SetActive(false);
        _typeInputfield.text = "";
    }
    private void SetActiveErrorList(bool isActive)
    {
        _titleErrorLabel.gameObject.SetActive(isActive);
        _typeErrorLabel.gameObject.SetActive(isActive);
        _descriptionErrorLabel.gameObject.SetActive(isActive);
        _effortErrorLabel.gameObject.SetActive(isActive);
    }
    private void SaveClick()
    {
        SetActiveErrorList(false);

        List<Text> errorList = new List<Text>();

        if (_titleInputfield.text == "")
            errorList.Add(_titleErrorLabel);
        if (_descriptionInputfield.text == "")
            errorList.Add(_descriptionErrorLabel);
        if (_effortInputfield.text == "")
            errorList.Add(_effortErrorLabel);
        if(_typeDropbox.value==0)
            errorList.Add(_typeErrorLabel);


        if (errorList.Count>0)
        {
            foreach(var item in errorList)
            {
                item.gameObject.SetActive(true);
            }
        }
        else
        {
            _okInSaveBG.SetActive(true);
        }


    }
    private void DropDownTypeChanged(int idx)
    {
        if(idx>0)
        {
            _typeInputfield.text = _typeDropbox.options[idx].text;
        }
    }
}
